use std::{env::args, process::exit, str::FromStr, time::Instant};

use bitvec::vec::BitVec;

type D = u8;
type DD = u16;
// type D4 = u32;

#[derive(Debug, Clone)] // clone needed for future parallelization
struct NiceNumberSearcher {
    base: D,
    max_digits: usize,
    fulfilled: BitVec,
    r: Vec<D>,
    r2: Vec<D>,
    r3: Vec<D>,
}

fn triangular(n: usize) -> usize {
    n * (n + 1) / 2
}

fn add_and_return_carry(base: D, slot: &mut D, added: D) -> D {
    let new_dig = (*slot as DD) + (added as DD);
    *slot = (new_dig % (base as DD)) as D;
    (new_dig / (base as DD)) as D
}

fn add_scaled(base: D, dest: &mut [D], src: &[D], scale: D) {
    let mut carry = 0;
    for i in 0..src.len() {
        // Add the appropriate digit times $2 d$
        let amount = (scale as DD) * (src[i] as DD) + carry;
        let added = (amount % (base as DD)) as D;
        let overflow = add_and_return_carry(base, &mut dest[i], added);
        carry = amount / (base as DD) + (overflow as DD);
    }
    add_constant(base, &mut dest[src.len()..], carry);
}

fn add_constant(base: D, dest: &mut [D], mut n: DD) {
    let mut i = 0;
    while n != 0 {
        let added = (n % (base as DD)) as D;
        let overflow = add_and_return_carry(base, &mut dest[i], added);
        n = n / (base as DD) + (overflow as DD);
        i += 1;
    }
}

impl NiceNumberSearcher {
    fn new(base: D) -> Self {
        let k = (base / 5) as usize;
        let max_digits = match base % 5 {
            0 => k,
            1 => 0,
            _ => k + 1,
        };
        let max_digits_triangular = triangular(max_digits);
        NiceNumberSearcher {
            base,
            max_digits,
            fulfilled: BitVec::repeat(false, base as usize),
            r: vec![0; max_digits],
            // Expanding $r$ by an extra digit might cause relevant
            // digits of $r^2$ and $r^3$ to be affected.
            // So we have to store the results for all suffixes of
            // $r$ under consideration.
            r2: vec![0; 2 * max_digits_triangular],
            r3: vec![0; 3 * max_digits_triangular],
        }
    }

    fn search_aux(&mut self, callback: &mut impl FnMut(&[D]), j: usize) {
        if j >= self.max_digits {
            return;
        }

        let tj = triangular(j);
        let tjm1 = if j == 0 { 0 } else { triangular(j - 1) };
        let (r2src, r2dest) = self.r2.split_at_mut(2 * tj);
        let (r3src, r3dest) = self.r3.split_at_mut(3 * tj);
        let r2src = &r2src[2 * tjm1..];
        let r3src = &r3src[3 * tjm1..];

        r2dest[..r2src.len()].copy_from_slice(r2src);
        r3dest[..r3src.len()].copy_from_slice(r3src);
        r2dest[r2src.len()..r2src.len() + 2].fill(0);
        r3dest[r3src.len()..r3src.len() + 3].fill(0);

        self.r[j] = 0;
        self.search_aux(callback, j + 1); // digit = 0

        'digits: for digit in 1..self.base {
            let (_r2src, r2dest) = self.r2.split_at_mut(2 * tj);
            let (_r3src, r3dest) = self.r3.split_at_mut(3 * tj);

            // Here, $r$ is the $r'$ from the previous iteration,
            // or the original $r$ if this is the first.

            // $r'^3 = b^{3j} + 3 r b^{2j} + 3 r^2 b^j + r^3$
            add_scaled(self.base, &mut r3dest[j..], &r2dest[..2 * j + 2], 3);
            add_scaled(self.base, &mut r3dest[2 * j..], &self.r[..j + 1], 3);
            add_constant(self.base, &mut r3dest[3 * j..], 1);

            // $r'^2 = b^{2j} + 2 r b^j + r^2$
            add_scaled(self.base, &mut r2dest[j..], &self.r[..j + 1], 2);
            add_constant(self.base, &mut r2dest[2 * j..], 1);

            // $r' = b^j + r$
            self.r[j] = digit;

            // Do the last `j` digits of $r^2$ and $r^3$ have duplicates?
            self.fulfilled.fill(false);
            for d in &r2dest[..j] {
                if self.fulfilled.replace(*d as usize, true) {
                    continue 'digits;
                }
            }
            for d in &r3dest[..j] {
                if self.fulfilled.replace(*d as usize, true) {
                    continue 'digits;
                }
            }
            // No, so check if all other digits are unique.
            let has_duplicates = 'duplicate_check: {
                let mut count = false;
                for d in Iterator::chain(r2dest[j..].iter().rev(), r3dest[j..].iter().rev()) {
                    count |= *d != 0;
                    if !count {
                        continue;
                    }
                    if self.fulfilled.replace(*d as usize, true) {
                        break 'duplicate_check true;
                    }
                }
                false
            };
            if !has_duplicates && self.fulfilled.all() {
                callback(&self.r[..j + 1]);
                return;
            }
            // If we were not previously disqualified, then search further
            self.search_aux(callback, j + 1);
        }
    }

    fn search(&mut self, mut callback: impl FnMut(&[D])) {
        self.search_aux(&mut callback, 0)
    }
}

fn main() {
    let Some(base) = args().nth(1) else {
        eprintln!("Usage: nice-dfs <base>");
        exit(1);
    };
    let Ok(base) = D::from_str(&base) else {
        eprintln!("Usage: nice-dfs <base>");
        exit(1);
    };
    let start = Instant::now();
    let mut searcher = NiceNumberSearcher::new(base);
    searcher.search(|r| {
        println!("Found nice number: {r:?}");
    });
    let end = Instant::now();
    let diff = end - start;
    println!("Completed in {}s", diff.as_secs_f64());
}
